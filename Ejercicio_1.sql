DROP DATABASE IF EXISTS Ejercicio_1;
CREATE DATABASE Ejercicio_1;

USE Ejercicio_3;

CREATE TABLE PERSONA(Nombre VARCHAR(20), Apellidos VARCHAR(40), Trabajo VARCHAR(30), PRIMARY KEY(Nombre, Apellidos));
CREATE TABLE OBJETO(Nombre VARCHAR(20), Tamaño ENUM('pequeño', 'mediano', 'grande'), NombreOBJETOcontenedor VARCHAR(20), PRIMARY KEY(Nombre), FOREIGN KEY(NombreOBJETOcontenedor) REFERENCES OBJETO(Nombre));
CREATE TABLE SITUACION(Hora TINYINT UNSIGNED, Lugar VARCHAR(30), NombrePERSONA VARCHAR(20), ApellidosPERSONA VARCHAR(40), vestuario VARCHAR(20), mercancia VARCHAR(20), PRIMARY KEY(Hora), FOREIGN KEY (NombrePERSONA, ApellidosPERSONA) REFERENCES PERSONA(Nombre, Apellidos));
CREATE TABLE lleva(HoraSITUACION TINYINT UNSIGNED, NombreOBJETO VARCHAR(20), FOREIGN KEY(HoraSITUACION) REFERENCES SITUACION(Hora), FOREIGN KEY(NombreOBJETO) REFERENCES OBJETO(Nombre));
/*INSERCION DE DATOS*/
INSERT INTO PERSONA VALUES 
('Skyler', 'White', 'Contable'),
('Jessie', 'Pinkman', 'Dealer'),
('Saul', 'Goodman', 'Abogado');

INSERT INTO OBJETO VALUES
('coche', 'grande', NULL),
('Mochila', 'mediano', 'coche'),
('Pistola', 'pequeño', 'Mochila');

INSERT INTO SITUACION VALUES
(9, 'Caravana', 'Jessie', 'Pinkman', 'Heisenberg', 'Baby blue'),
(10, 'Despacho', 'Saul', 'Goodman', 'Traje', 'Pasta $$$');

INSERT INTO lleva VALUES
(9, 'Pistola'),
(10, 'Mochila');
