DROP DATABASE IF EXISTS ExamenStarkTrek;
CREATE DATABASE ExamenStarkTrek;
USE ExamenStarkTrek;

CREATE TABLE Actores (
Nombre VARCHAR(20),
Personaje VARCHAR(30),
FechaNacimiento DATE,
Nacionalidad VARCHAR(20),

CONSTRAINT PK_Actores PRIMARY KEY(Nombre)
);

CREATE TABLE Personajes (
Nombre VARCHAR(30),
Raza VARCHAR(20),
GraduacionMilitar ENUM('Capitan', 'Teniente', 'Almirante', 'Oficial'),
NombreActor VARCHAR(20),
NombrePersonajes VARCHAR(30),

CONSTRAINT PK_Personajes PRIMARY KEY(Nombre)
CONSTRAINT FK_PersonajesActores FOREIGN KEY (NombreActor) REFERENCES Actores(Nombre),
CONSTRAINT FK_PersonajesPersonajes FOREIGN KEY (NombrePersonajes) REFERENCES Personajes(Nombre)

);

CREATE TABLE Nave(
Nombre VARCHAR(30),
Codigo INT UNSIGNED,
NumTripulantes INT UNSIGNED,

CONSTRAINT PK_Naves PRIMARY KEY(Codigo)
); 




CREATE TABLE Planetas(
Codigo INT UNSIGNED,
Nombre VARCHAR(30),
Galaxia VARCHAR(40),
Problema VARCHAR(255),
CodigoNave INT UNSIGNED,

CONSTRAINT PK_Planetas PRIMARY KEY (Codigo),
CONSTRAINT FK_Planetas_Naves FOREING KEY (CodigoNave) REFERENCES Naves (Codigo)
);


CREATE TABLE Capitulos (
Numero INT UNSIGNED,
Temporada INT UNSIGNED,
Titulo VARCHAR(50),
Orden INT UNSIGNED,
CodigoPlanetas INT UNSIGNED,

CONSTRAINT PK_Capitulos PRIMARY KEY(Numero, Temporada),
CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlaneta) REFERENCES Planetas(Codigo)
);

CREATE TABLE AparicionesSerie (
NombrePersonaje VARCHAR(30),
NumeroCapitulo INT UNSIGNED,
NumeroTemporada INT UNSIGNED

CONSTRAINT FK_AparicionesSeriePersonajes FOREIGN KEY (NombrePersonaje) REFERENCES Personajes(Nombre),
CONSTRAINT FK_AparicionesSerieCapitulo FOREIGN KEY (NumeroCapitulo, NumeroTemporada) REFERENCES Capitulos(Numero, Temporada)
);

CREATE TABLE Peliculas (
Titulo VARCHAR(30),
AñoLanzamiento YEAR,
Director VARCHAR(50),

CONSTRAINT PK_PELICULAS PRIMARY KEY (TITULO) 
);


CREATE TABLE AparicionesPelis(
NombrePersonaje VARCHAR(30),
TituloPelicula VARCHAR(30),
Protagonista ENUM('SI', 'NO')

CONSTRAINT FK_AparicionesPelisPersonajes FOREIGN KEY (NombrePersonaje) REFERENCES Personajes(Nombre),
CONSTRAINT FK_AparicionesPelisPelis FOREIGN KEY (TituloPelicula) REFERENCES Peliculas(Titulo)

);

/*MODIF*/

ALTER TABLE Planetas DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Planetas MODIFY Codigo INT UNSIGNED AUTO_INCREMENT;
ALTER TABLE Planetas ADD CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlaneta) REFERENCES Planetas(Codigo);


/*FORIGN KEY CASCADE*/

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Capitulos MODIFY CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlaneta) REFERENCES Planetas(Codigo) ON UPDATE CASCADE;


/*Añadir columna */

ALTER TABLE Peliculas ADD Recaudacion DECIMAL(5,2);


/*INSERT*/

INSERT INTO Actores VALUES ('Luke Perry', 'Spiderman', '1990-01-01', 'Turca');

INSERT INTO Actores VALUES ('Galindo', 'Ant-man', '1930-01-01', 'Murciana');

INSERT INTO Personajes VALUES ('Spiderman', 'Mutante', 'Oficial', 'Luke Perry', NULL);

INSERT INTO Naves VALUES ('Halcon Milenario', 1, 10);

INSERT INTO Planetas (Nombre, Galaxia, Problema, CodigoNave) VALUES ('Kamino', 'Via Lactea', 'Nos enfrentamos mi capitan, mi capitan...', 1);

INSERT INTO Capitulos VALUES (1, 1, 'The Beginning Is th End Is The Beginning', 3, 1);

INSERT INTO Capitulos VALUES (1, 2, '2X01', 1, 1);

INSERT INTO AparicionesSerie  VALUES ('Spiderman', 1, 1);

INSERT INTO Peliculas VALUES ('Shin-Chan y las bolas magicas', 2007, 'NiKito NiPongo LaPelota', 123.25);

INSERT INTO AparicionesPeli  VALUES ('Spiderman', 'Shin-Chan y las bolas magicas', 'SI');


/*ACTUALIZAR*/

UPDATE AparicionesSeries SET NumeroTemporada=2 WHERE NumeroCapitulo=1 AND NumeroTemporada=1; 


/*DROP*/

DELETE FROM Actores WHERE Nombre ='Luke Perry';


/*usuario*/

CREATE USER 'usuario' @ 'localhost' identified by 'usuario';
GRANT SELECT(COLUMNAS) ON Planetas TO 'usuario'@'localhost'

REVOKE ALL PRIVILEGIES FROM 'usuario'@'localhost';
