INSERT INTO SOCIOS (Nombre, Direccion, NumeroTelefono, FechaInscripcion) VALUES 
('Pedro Gil', 'Canelones', '123.456.789', '1984-12-12'),
('Jose M. Foro', 'Montevideo', '987.654.321','1997-03-02'),
('Elba lazo', 'Las Piedras', '666.777.888', '2000-06-02'),
('Marta Cana', 'Montevideo', '600.200.200', '1999-08-15');

INSERT INTO LIBROS (Titulo, Autor, FechaEditado) VALUES 
('El meteorologo', 'Aitor Menta', '1954-12-12'),
('La Fiesta', 'Encarna Vales', '1987-03-02'),
('El Golpe', 'Marcos Corro', '1990-06-02'),
('La furia', 'Elbio Lento', '1994-12-25');

INSERT INTO PRESTAMOS (FechaRetiro, FechaEntrega, NumeroSocio, NumeroLibro) VALUES
('2003-12-12', '2003-12-22', 1, 1),
('2003-02-02', '2003-02-12', 2, 1),
('2003-02-03', '2003-02-12', 3, 2),
('2003-08-03', '2003-08-13', 4, 4);
