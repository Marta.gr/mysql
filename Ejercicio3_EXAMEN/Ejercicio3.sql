DROP DATABASE IF EXISTS EJERCICIO3;
CREATE DATABASE EJERCICIO3;
USE EJERCICIO3;

CREATE TABLE SOCIOS(
	Nombre VARCHAR(20),
	Direccion VARCHAR(30),
	NumeroTelefono VARCHAR(15),
	FechaInscripcion DATE,
	NumeroSocio INT AUTO_INCREMENT,
	CONSTRAINT PK_SOCIOS PRIMARY KEY(NumeroSocio));

CREATE TABLE LIBROS(
	Titulo VARCHAR(30),
	Autor VARCHAR(20),
	FechaEditado DATE,
	NumeroLibro BIGINT AUTO_INCREMENT,
	CONSTRAINT PK_LIBROS PRIMARY KEY(NumeroLibro));

CREATE TABLE PRESTAMOS(
	FechaRetiro DATE,
	FechaEntrega DATE,
	NumeroSocio INT, 
	NumeroLibro BIGINT, 
	IdPrestamo BIGINT AUTO_INCREMENT,
	CONSTRAINT PK_PRESTAMOS PRIMARY KEY(IdPrestamo));

ALTER TABLE PRESTAMOS ADD CONSTRAINT FK_NumeroSocio_PRESTAMOS FOREIGN KEY (NumeroSocio) REFERENCES SOCIOS(NumeroSocio);
ALTER TABLE PRESTAMOS ADD CONSTRAINT FK_NumeroLibro_PRESTAMOS FOREIGN KEY (NumeroLibro) REFERENCES LIBROS(NumeroLibro);

/* PARA BORRAR EL SOCIO Nº */
/*DELETE FROM SOCIOS WHERE NumeroSocio=4;*/


