DROP DATABASE IF EXISTS Ejercicio1;
CREATE DATABASE Ejercicio1;
USE Ejercicio1;

CREATE TABLE CLIENTES (
  DNI char(9) NOT NULL,
  nombre varchar(20),
  apellidos varchar(40),
  telefono char(9),
  eMail varchar(50) UNIQUE,
  CONSTRAINT PK_CLIENTES PRIMARY KEY (DNI)
);

CREATE TABLE TIENDAS(
  nombre varchar(20),
  provincia varchar(20),
  localidad varchar(20),
  direccion varchar(40),
  telefono char(9),
  diaApertura ENUM ('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'),
  diaCierre ENUM ('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'),
  horaApertura tinyint unsigned,
  horaCierre tinyint unsigned,
  CONSTRAINT  PK_TIENDAS PRIMARY KEY (nombre)
);

CREATE TABLE OPERADORAS(
  nombre varchar(20),
  colorLogo varchar(10),
  porcentajeCobertura tinyint UNSIGNED COMMENT 'UNIDADES EN %',
  frecuenciaGSM SMALLINT UNSIGNED,
  paginaWEB varchar(100),
  CONSTRAINT PK_OPERADORAS PRIMARY KEY (nombre)
);

CREATE TABLE TARIFAS (
  nombre varchar(20),
  nombre_OPERADORAS varchar(20),
  tamanoDatos TINYINT UNSIGNED,
  tipoDatos ENUM('MB', 'GB', 'TB'),
  minutosGratis SMALLINT,
  SMSGratis ENUM ('SI', 'NO'),
  CONSTRAINT PK_TARIFAS PRIMARY KEY (nombre, nombre_OPERADORAS),
  CONSTRAINT FK_TARIFAS FOREIGN KEY (nombre_OPERADORAS) REFERENCES OPERADORAS(nombre)
);

CREATE TABLE MOVILES (
  marca varchar(20),
  modelo varchar(30),
  descripción varchar(300),
  SO enum ('iOS', 'Android'),
  RAM tinyint UNSIGNED,
  pulgadasPantalla DECIMAL (3,2)UNSIGNED,
  camaraMPX DECIMAL (2,2) UNSIGNED,
  CONSTRAINT PK_MOVILES PRIMARY KEY (marca,modelo)
);
CREATE TABLE MOVIL_LIBRE(
  marca_MOVILES varchar(20),
  modelo_MOVILES varchar(10),
  precio float,
  PRIMARY KEY (modelo_MOVILES),
  FOREIGN KEY (marca_MOVILES, modelo_MOVILES) REFERENCES MOVILES (marca, modelo)
);

CREATE TABLE MOVIL_CONTRATO (
  marca_MOVILES varchar(20),
  modelo_MOVILES varchar(30),
  nombre_OPERADORAS varchar(20),
  precio DECIMAL (6,2),
  PRIMARY KEY (marca_MOVILES, marca_MOVILES, nombre_OPERADORAS),
  CONSTRAINT FK_CONTRATO_MOVILES FOREIGN KEY (marca_MOVILES, modelo_MOVILES) REFERENCES MOVILES (marca, modelo),
  CONSTRAINT FK_CONTRATO_OPERADORAS FOREIGN KEY (nombre_OPERADORAS) REFERENCES OPERADORAS (nombre)
);

CREATE TABLE OFERTAS(
  nombre_OPERADORAS_TARIFAS varchar(20),
  nombre_TARIFAS varchar(50),
  marca_MOVIL_CONTRATO varchar(20),
  modelo_MOVIL_CONTRATO varchar(30),
  PRIMARY KEY (nombre_TARIFAS, marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO),
  CONSTRAINT FK_OFERTAS_TARIFAS FOREIGN KEY (nombre_TARIFAS) REFERENCES TARIFAS(nombre),
  FOREIGN KEY (marca_MOVIL_CONTRATO) REFERENCES MOVIL_CONTRATO(marca),
  FOREIGN KEY (modelo_MOVIL_CONTRATO) REFERENCES MOVIL_CONTRATO(modelo)
);

CREATE TABLE COMPRAS (
  DNI_CLIENTE varchar(20),
  nombre_TIENDA varchar(10),
  marca_MOVILES_LIBRE varchar(10),
  modelo_MOVIL_LIBRE varchar(20),
  dia date,
  PRIMARY KEY (DNI_CLIENTE, nombre_TIENDA, marca_MOVIL_LIBRE, modelo_MOVIL_LIBRE, dia),
  FOREIGN KEY (nombre_TIENDA) REFERENCES TIENDA(nombre),
  FOREIGN KEY (DNI_CLIENTE) REFERENCES CLIENTE(DNI),
  FOREIGN KEY (marca_MOVIL_LIBRE) REFERENCES MOVIL_LIBRE(marca),
  FOREIGN KEY (modelo_MOVIL_LIBRE) REFERENCES MOVILES_LIBRE(modelo)
);


CREATE TABLE CONTRATOS (
  DNI_CLIENTE varchar(20),
  nombre_TIENDA varchar(10),
  nombre_OPERADORAS_TARIFAS_OFERTAS varchar(20),
  nombre_TARIFAS_OFERTAS varchar(10),
  marca_MOVILES_OFERTAS varchar(20),
  modelo_MOVILES_OFERTAS varchar(20),
  Dia date
  PRIMARY KEY (DNI_CLIENTE, nombre_TIENDA, nombre_OPERADORAS_TARIFAS_OFERTAS,nombre_TARIFAS_OFERTAS,    marca_MOVILES_OFERTAS, modelo_MOVILES_OFERTAS),
  FOREIGN KEY (DNI_CLIENTES) REFERENCES CLIENTES(DNI),
  FOREIGN KEY (nombre_TIENDA) REFERENCES TIENDA(nombre),
  FOREIGN KEY (nombre_OPERADORAS_TARIFAS_OFERTAS) REFERENCES OFERTAS(nombre_OPERADORAS_TARIFA),
  FOREIGN KEY (marca_MOVILES_OFERTAS) REFERENCES MOVIL_LIBRE(marca),
  FOREIGN KEY (modelo_MOVILES_OFERTAS) REFERENCES MOVILES_LIBRE(modelo)
);

INSERT INTO CLIENTES VALUES
    ('02302350H', 'Perico', 'De lo palotes', '654862586', 'perico@todo.com'),
    ('16874654G', 'Fulanito', 'De tal', '65494687', 'ful@nito.com')
;

insert into TIENDAS VALUES
    ('Gran Vía', 'Madrid', 'Madrid', 'c/Gran Vía 32', '16844646', 'Lunes' , 'viernes' , 9,18),
    ('Castellana', ' Madrid', 'Madrid', ' c/Castellana 108','654968548', 'Lunes', 'Sabado', 8, 15)
    ;
