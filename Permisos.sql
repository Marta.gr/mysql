drop database if exists Permisos;
create database Permisos;
USE Permisos;

create table clientes(
    ID int auto_increment,
    dni char(9),
    nombre varchar(20),
    primer_apellido varchar(20),
    segundo_apellido varchar(20),
    direccion varchar(50),
    telefono char(9),

constraint PK_CLIENTES PRIMARY KEY (ID)
);

create table productos(
    ID int auto_increment,
    precio int,
    fabricante varchar(40),
    fecha_fabricacion date,
    fecha_caducidad date,
    cantidad int,

constraint PK_PRODUCTOS PRIMARY KEY(ID)
);


create table pedidos(
    ID int auto_increment,
    ID_cliente int,
    ID_producto int,
    fecha_pedido date,
    fecha_entrega date,
constraint PK_pedidos PRIMARY KEY(ID),
constraint FK_cliente_pedidos FOREIGN KEY (ID_cliente) references clientes (ID),
constraint FK_producto_cliente FOREIGN KEY (ID_producto) references productos (ID)
);

