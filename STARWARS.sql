-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: STARWARS
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ACTORES`
--

DROP TABLE IF EXISTS `ACTORES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACTORES` (
  `Codigo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `NACIONALIDAD` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACTORES`
--

LOCK TABLES `ACTORES` WRITE;
/*!40000 ALTER TABLE `ACTORES` DISABLE KEYS */;
INSERT INTO `ACTORES` VALUES (1,'Carrie Fisher','1956-10-21','Estadounidense'),(2,'Mark Hamill','1951-09-25','Estadounidense'),(3,'Peter Mayhew','1944-05-19','Inglés'),(4,'Kenny Baker','1934-08-24','Inglés'),(5,'Adam Driver','1983-11-19','Estadounidense');
/*!40000 ALTER TABLE `ACTORES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NAVES`
--

DROP TABLE IF EXISTS `NAVES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NAVES` (
  `Codigo` int(11) NOT NULL AUTO_INCREMENT,
  `NºTripulantes` int(11) DEFAULT NULL,
  `Nombre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NAVES`
--

LOCK TABLES `NAVES` WRITE;
/*!40000 ALTER TABLE `NAVES` DISABLE KEYS */;
INSERT INTO `NAVES` VALUES (1,1000,'Malevolencia'),(2,600,'Mano Invisible'),(3,8,'Halcón de Ébano'),(4,6,'YT-2400'),(5,1,'Halcón Milenario');
/*!40000 ALTER TABLE `NAVES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PELICULAS`
--

DROP TABLE IF EXISTS `PELICULAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PELICULAS` (
  `Codigo` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(40) DEFAULT NULL,
  `Director` varchar(40) DEFAULT NULL,
  `Año` year(4) DEFAULT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PELICULAS`
--

LOCK TABLES `PELICULAS` WRITE;
/*!40000 ALTER TABLE `PELICULAS` DISABLE KEYS */;
INSERT INTO `PELICULAS` VALUES (1,'StarWars La Amenaza Fantasma','George Lucas',1999),(2,'StarWars El ataque de los clones','George Lucas',2002),(3,'StarWars El imperio contraataca','Irvin Kershner',1980),(4,'StarWars El retorno del jedi','Richard Marquand',1983),(5,'StarWars La guerra de los clones','George Lucas',2008);
/*!40000 ALTER TABLE `PELICULAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERSONAJES`
--

DROP TABLE IF EXISTS `PERSONAJES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSONAJES` (
  `Codigo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) DEFAULT NULL,
  `Raza` varchar(20) DEFAULT NULL,
  `GRADO` smallint(6) DEFAULT NULL,
  `Codigo_ACTORES` int(11) NOT NULL,
  `CodigoSuperior_PERSONAJES` int(11) NOT NULL,
  PRIMARY KEY (`Codigo`,`Codigo_ACTORES`,`CodigoSuperior_PERSONAJES`),
  KEY `FK_ACTORES` (`Codigo_ACTORES`),
  KEY `FK_PERSONAJES` (`CodigoSuperior_PERSONAJES`),
  CONSTRAINT `FK_ACTORES` FOREIGN KEY (`Codigo_ACTORES`) REFERENCES `ACTORES` (`Codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_PERSONAJES` FOREIGN KEY (`CodigoSuperior_PERSONAJES`) REFERENCES `PERSONAJES` (`Codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERSONAJES`
--

LOCK TABLES `PERSONAJES` WRITE;
/*!40000 ALTER TABLE `PERSONAJES` DISABLE KEYS */;
INSERT INTO `PERSONAJES` VALUES (1,'Princesa Leia','Humano',6,1,0),(2,'Luke Skywalker','Humano',9,2,1),(3,'Chewbacca','Monstruo',1,3,2),(4,'Kenny Baker','Robot',10,4,3),(5,'Kylo Ren','Humano',11,5,4);
/*!40000 ALTER TABLE `PERSONAJES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERSONAJESPELICULAS`
--

DROP TABLE IF EXISTS `PERSONAJESPELICULAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSONAJESPELICULAS` (
  `Codigo_PERSONAJES` int(11) NOT NULL,
  `Codigo_PELICULAS` int(11) NOT NULL,
  PRIMARY KEY (`Codigo_PERSONAJES`,`Codigo_PELICULAS`),
  KEY `FK_PELICULAS` (`Codigo_PELICULAS`),
  CONSTRAINT `FK_PELICULAS` FOREIGN KEY (`Codigo_PELICULAS`) REFERENCES `PELICULAS` (`Codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_PERSONAJES_PERSONAJESPELICULAS` FOREIGN KEY (`Codigo_PERSONAJES`) REFERENCES `PERSONAJES` (`Codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERSONAJESPELICULAS`
--

LOCK TABLES `PERSONAJESPELICULAS` WRITE;
/*!40000 ALTER TABLE `PERSONAJESPELICULAS` DISABLE KEYS */;
INSERT INTO `PERSONAJESPELICULAS` VALUES (2,3),(1,4),(2,4),(1,5),(4,5);
/*!40000 ALTER TABLE `PERSONAJESPELICULAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PLANETAS`
--

DROP TABLE IF EXISTS `PLANETAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PLANETAS` (
  `Codigo` int(11) NOT NULL AUTO_INCREMENT,
  `Galaxia` varchar(40) DEFAULT NULL,
  `Nombre` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PLANETAS`
--

LOCK TABLES `PLANETAS` WRITE;
/*!40000 ALTER TABLE `PLANETAS` DISABLE KEYS */;
INSERT INTO `PLANETAS` VALUES (1,'Mundos del Núcleo','Alderaan'),(2,'Territorios del Borde Exterior','Christophsis'),(3,'Borde Interior','Takodana'),(4,'Espacio Salvaje','Lirasan'),(5,'Borde Exterior','Karkaris');
/*!40000 ALTER TABLE `PLANETAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VISITAS`
--

DROP TABLE IF EXISTS `VISITAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VISITAS` (
  `Codigo_NAVES` int(11) NOT NULL,
  `Codigo_PLANETAS` int(11) NOT NULL,
  `Codigo_PELICULAS` int(11) NOT NULL,
  PRIMARY KEY (`Codigo_NAVES`,`Codigo_PLANETAS`,`Codigo_PELICULAS`),
  KEY `FK_PLANETAS` (`Codigo_PLANETAS`),
  KEY `FK_PELICULAS_VISITAS` (`Codigo_PELICULAS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VISITAS`
--

LOCK TABLES `VISITAS` WRITE;
/*!40000 ALTER TABLE `VISITAS` DISABLE KEYS */;
INSERT INTO `VISITAS` VALUES (2,3,1),(2,3,4),(4,4,2),(5,4,3),(1,5,5);
/*!40000 ALTER TABLE `VISITAS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-20 13:27:55
