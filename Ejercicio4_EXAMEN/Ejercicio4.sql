DROP DATABASE IF EXISTS Starwars;
CREATE DATABASE Starwars;
USE Starwars;

CREATE TABLE Actores(
	Codigo INTEGER PRIMARY KEY,
	Nombre VARCHAR(40),
	Fecha DATE,
	Nacionalidad VARCHAR(20));

CREATE TABLE Personajes(
	Codigo INTEGER PRIMARY KEY,
	Nombre VARCHAR(30), 
	Raza VARCHAR(20),
	Grado VARCHAR(20),
	CodigoActor INTEGER);

ALTER TABLE Personajes ADD CONSTRAINT FK_Personajes FOREIGN KEY (CodigoActor) REFERENCES Actores(Codigo) ON UPDATE CASCADE ON DELETE CASCADE;

/* update */ 
/* UPDATE ACTORES SET Codigo='213' WHERE Codigo='212'; */

/* delete */
/* DELETE FROM Actores WHERE Codigo='213'*/

/*Select*/
/* SELECT Codigo AS Numero, Nombre, CodigoActor FROM Personajes LIMIT 2; */

