INSERT INTO Actores VALUES
(210, 'Brad Pitt', '2019-01-17', 'EstadoUnidense'),
(211, 'Leonardo Di Caprio', '2019-01-16', 'EstadoUnidense'),
(212, 'Dove Cameron', '2019-01-15', 'De Estados Unidos');

INSERT INTO Personajes VALUES
(456, 'Darth Vader', 'Hombre', 'Medio', 210),
(457, 'R2 D2', 'Robot', 'Superior', 211),
(458, 'Chewbacca', 'Mono', 'Extremo', 212);

