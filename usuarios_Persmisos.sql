DROP USER IF EXISTS 'clientes'@'localhost';
DROP USER IF EXISTS 'comercial'@'localhost';
DROP USER IF EXISTS 'administrador'@'localhost';
DROP USER IF EXISTS 'gerente'@'localhost';
DROP USER IF EXISTS 'cajero'@'localhost';



CREATE USER 'clientes'@'localhost' IDENTIFIED BY 'clientes';
CREATE USER 'comercial'@'localhost' IDENTIFIED BY 'comercial';
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'administrador';
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'gerente';
CREATE USER 'cajero'@'localhost' IDENTIFIED BY 'cajero';


