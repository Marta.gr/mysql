-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: moviles
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CLIENTES`
--

DROP TABLE IF EXISTS `CLIENTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CLIENTES` (
  `DNI` char(9) NOT NULL,
  `Nombre` varchar(20) DEFAULT NULL,
  `Apellidos` varchar(30) DEFAULT NULL,
  `Telefono` char(9) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENTES`
--

LOCK TABLES `CLIENTES` WRITE;
/*!40000 ALTER TABLE `CLIENTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPRAS`
--

DROP TABLE IF EXISTS `COMPRAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPRAS` (
  `DNI_CLIENTES` char(9) NOT NULL,
  `Nombre_TIENDAS` varchar(20) NOT NULL,
  `Marca_MOVIL_LIBRE` varchar(20) NOT NULL,
  `Modelo_MOVIL_LIBRE` varchar(20) NOT NULL,
  `DIA` date DEFAULT NULL,
  PRIMARY KEY (`DNI_CLIENTES`,`Nombre_TIENDAS`,`Marca_MOVIL_LIBRE`,`Modelo_MOVIL_LIBRE`),
  KEY `FK_TIENDAS_COMPRAS` (`Nombre_TIENDAS`),
  KEY `FK_MOVIL_LIBRE` (`Marca_MOVIL_LIBRE`,`Modelo_MOVIL_LIBRE`),
  CONSTRAINT `FK_CLIENTES_COMPRAS` FOREIGN KEY (`DNI_CLIENTES`) REFERENCES `CLIENTES` (`DNI`),
  CONSTRAINT `FK_MOVIL_LIBRE` FOREIGN KEY (`Marca_MOVIL_LIBRE`, `Modelo_MOVIL_LIBRE`) REFERENCES `MOVIL_LIBRE` (`Marca_MOVIL`, `Modelo_MOVIL`),
  CONSTRAINT `FK_TIENDAS_COMPRAS` FOREIGN KEY (`Nombre_TIENDAS`) REFERENCES `TIENDAS` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPRAS`
--

LOCK TABLES `COMPRAS` WRITE;
/*!40000 ALTER TABLE `COMPRAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPRAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONTRATOS`
--

DROP TABLE IF EXISTS `CONTRATOS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTRATOS` (
  `DNI_CLIENTES` char(9) NOT NULL,
  `Nombre_TIENDAS` varchar(20) NOT NULL,
  `Nombre_OPERADORAS_TARIFAS_OFERTAS` varchar(20) NOT NULL,
  `Nombre_TARIFAS_OFERTAS` varchar(20) NOT NULL,
  `Marca_MOVIL_OFERTAS` varchar(20) NOT NULL,
  `Modelo_MOVIL_OFERTAS` varchar(20) NOT NULL,
  `Dia` date DEFAULT NULL,
  PRIMARY KEY (`DNI_CLIENTES`,`Nombre_TIENDAS`,`Nombre_OPERADORAS_TARIFAS_OFERTAS`,`Nombre_TARIFAS_OFERTAS`,`Marca_MOVIL_OFERTAS`,`Modelo_MOVIL_OFERTAS`),
  KEY `FK_TIENDAS_CONTRATOS` (`Nombre_TIENDAS`),
  KEY `FK_OFERTAS` (`Nombre_OPERADORAS_TARIFAS_OFERTAS`,`Nombre_TARIFAS_OFERTAS`,`Marca_MOVIL_OFERTAS`,`Modelo_MOVIL_OFERTAS`),
  CONSTRAINT `FK_CLIENTES_CONTRATOS` FOREIGN KEY (`DNI_CLIENTES`) REFERENCES `CLIENTES` (`DNI`),
  CONSTRAINT `FK_OFERTAS` FOREIGN KEY (`Nombre_OPERADORAS_TARIFAS_OFERTAS`, `Nombre_TARIFAS_OFERTAS`, `Marca_MOVIL_OFERTAS`, `Modelo_MOVIL_OFERTAS`) REFERENCES `OFERTAS` (`Nombre_OPERADORAS_TARIFAS`, `Nombre_TARIFAS`, `Marca_MOVIL_CONTRATO`, `Modelo_MOVIL_CONTRATO`),
  CONSTRAINT `FK_TIENDAS_CONTRATOS` FOREIGN KEY (`Nombre_TIENDAS`) REFERENCES `TIENDAS` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTRATOS`
--

LOCK TABLES `CONTRATOS` WRITE;
/*!40000 ALTER TABLE `CONTRATOS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CONTRATOS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MOVILES`
--

DROP TABLE IF EXISTS `MOVILES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MOVILES` (
  `Marca` varchar(20) NOT NULL,
  `Modelo` varchar(20) NOT NULL,
  `Descripción` varchar(50) DEFAULT NULL,
  `SO` varchar(20) DEFAULT NULL,
  `RAM` varchar(10) DEFAULT NULL,
  `PulgadasPantalla` tinyint(3) unsigned DEFAULT NULL,
  `CamaraMpx` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`Marca`,`Modelo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MOVILES`
--

LOCK TABLES `MOVILES` WRITE;
/*!40000 ALTER TABLE `MOVILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `MOVILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MOVIL_CONTRATO`
--

DROP TABLE IF EXISTS `MOVIL_CONTRATO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MOVIL_CONTRATO` (
  `Marca_MOVIL` varchar(20) NOT NULL,
  `Modelo_MOVIL` varchar(20) NOT NULL,
  `Nombre_OPERADORAS` varchar(20) DEFAULT NULL,
  `Precio` float DEFAULT NULL,
  PRIMARY KEY (`Marca_MOVIL`,`Modelo_MOVIL`),
  CONSTRAINT `FK_MOVILES_CONTRATO` FOREIGN KEY (`Marca_MOVIL`, `Modelo_MOVIL`) REFERENCES `MOVILES` (`Marca`, `Modelo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MOVIL_CONTRATO`
--

LOCK TABLES `MOVIL_CONTRATO` WRITE;
/*!40000 ALTER TABLE `MOVIL_CONTRATO` DISABLE KEYS */;
/*!40000 ALTER TABLE `MOVIL_CONTRATO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MOVIL_LIBRE`
--

DROP TABLE IF EXISTS `MOVIL_LIBRE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MOVIL_LIBRE` (
  `Marca_MOVIL` varchar(20) NOT NULL,
  `Modelo_MOVIL` varchar(20) NOT NULL,
  `Precio` float DEFAULT NULL,
  PRIMARY KEY (`Marca_MOVIL`,`Modelo_MOVIL`),
  CONSTRAINT `FK_MOVILES_LIBRE` FOREIGN KEY (`Marca_MOVIL`, `Modelo_MOVIL`) REFERENCES `MOVILES` (`Marca`, `Modelo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MOVIL_LIBRE`
--

LOCK TABLES `MOVIL_LIBRE` WRITE;
/*!40000 ALTER TABLE `MOVIL_LIBRE` DISABLE KEYS */;
/*!40000 ALTER TABLE `MOVIL_LIBRE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OFERTAS`
--

DROP TABLE IF EXISTS `OFERTAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OFERTAS` (
  `Nombre_OPERADORAS_TARIFAS` varchar(20) NOT NULL,
  `Nombre_TARIFAS` varchar(20) NOT NULL,
  `Marca_MOVIL_CONTRATO` varchar(20) NOT NULL,
  `Modelo_MOVIL_CONTRATO` varchar(20) NOT NULL,
  PRIMARY KEY (`Nombre_OPERADORAS_TARIFAS`,`Nombre_TARIFAS`,`Marca_MOVIL_CONTRATO`,`Modelo_MOVIL_CONTRATO`),
  KEY `FK_TARIFAS` (`Nombre_TARIFAS`),
  KEY `FK_MOVIL_CONTRATO` (`Marca_MOVIL_CONTRATO`,`Modelo_MOVIL_CONTRATO`),
  CONSTRAINT `FK_MOVIL_CONTRATO` FOREIGN KEY (`Marca_MOVIL_CONTRATO`, `Modelo_MOVIL_CONTRATO`) REFERENCES `MOVIL_CONTRATO` (`Marca_MOVIL`, `Modelo_MOVIL`),
  CONSTRAINT `FK_OPERADORAS_OFERTAS` FOREIGN KEY (`Nombre_OPERADORAS_TARIFAS`) REFERENCES `OPERADORAS` (`Nombre`),
  CONSTRAINT `FK_TARIFAS` FOREIGN KEY (`Nombre_TARIFAS`) REFERENCES `OPERADORAS` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OFERTAS`
--

LOCK TABLES `OFERTAS` WRITE;
/*!40000 ALTER TABLE `OFERTAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `OFERTAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OPERADORAS`
--

DROP TABLE IF EXISTS `OPERADORAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OPERADORAS` (
  `Nombre` varchar(20) NOT NULL,
  `ColorLogo` varchar(20) DEFAULT NULL,
  `PorcentajeCobertura` smallint(5) unsigned DEFAULT NULL,
  `FrecuenciaGSM` int(11) DEFAULT NULL,
  `PaginaWEB` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OPERADORAS`
--

LOCK TABLES `OPERADORAS` WRITE;
/*!40000 ALTER TABLE `OPERADORAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `OPERADORAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TARIFAS`
--

DROP TABLE IF EXISTS `TARIFAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TARIFAS` (
  `Nombre` varchar(20) NOT NULL,
  `Nombre_OPERADORAS` varchar(20) NOT NULL,
  `TamañoDatos` int(11) DEFAULT NULL,
  `TipoDatos` varchar(2) DEFAULT NULL,
  `MinutosGratis` tinyint(1) DEFAULT NULL,
  `SMSGratis` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Nombre`,`Nombre_OPERADORAS`),
  KEY `FK_OPERADORAS` (`Nombre_OPERADORAS`),
  CONSTRAINT `FK_OPERADORAS` FOREIGN KEY (`Nombre_OPERADORAS`) REFERENCES `OPERADORAS` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TARIFAS`
--

LOCK TABLES `TARIFAS` WRITE;
/*!40000 ALTER TABLE `TARIFAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `TARIFAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TIENDAS`
--

DROP TABLE IF EXISTS `TIENDAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TIENDAS` (
  `Nombre` varchar(20) NOT NULL,
  `Provincia` varchar(20) DEFAULT NULL,
  `Localidad` varchar(20) DEFAULT NULL,
  `Dirección` varchar(50) DEFAULT NULL,
  `Telefono` char(9) DEFAULT NULL,
  `DíaApertura` date DEFAULT NULL,
  `DíaCierre` date DEFAULT NULL,
  `HoraApertura` time DEFAULT NULL,
  `HoraCierre` time DEFAULT NULL,
  PRIMARY KEY (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TIENDAS`
--

LOCK TABLES `TIENDAS` WRITE;
/*!40000 ALTER TABLE `TIENDAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `TIENDAS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-20 13:33:52
