DROP DATABASE IF EXISTS Ejercicio2;
CREATE DATABASE Ejercicio2;
USE Ejercicio2;


CREATE TABLE ACTORES (
<<<<<<< HEAD
  codigo int(10) not null,
  nombre varchar(20),
=======
  codigo int NOT NULL AUTO_INCREMENT,
  nombre varchar(30),
>>>>>>> 0fa91b1089a1abe65085b93fb21938a025f29ff0
  fecha date,
  nacionalidad varchar(30) DEFAULT 'ESTADOUNIDENSE',
  CONSTRAINT PK_ACTORES PRIMARY KEY (codigo)
);

CREATE TABLE NAVES (
  codigo int(11) NOT NULL ,
  nºTripulantes int(11),
  nombre varchar(20),
  PRIMARY KEY (codigo)
);

CREATE TABLE PELICULAS (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(40),
  director varchar(40),
  año year(4),
  PRIMARY KEY (codigo)
);

CREATE TABLE PERSONAJES (
  codigo int NOT NULL AUTO_INCREMENT,
  nombre varchar(30),
  raza varchar(30) default 'Humano',
  grado smallint UNSIGNED,
  codigo_ACTORES int,
  codigoSuperior_PERSONAJES int,
  CONSTRAINT PK_PERSONAJES PRIMARY KEY (codigo),
  CONSTRAINT FK_ACTORES_PERSONAJES FOREIGN KEY (codigo_ACTORES) REFERENCES ACTORES (codigo),
  CONSTRAINT FK_PERSONAJES_PERSONAJES FOREIGN KEY (codigoSuperior_PERSONAJES) REFERENCES PERSONAJES (codigo)
);

CREATE TABLE PERSONAJES_PELICULAS (
  codigo_PERSONAJES int(11) NOT NULL,
  codigo_PELICULAS int(11) NOT NULL,
  PRIMARY KEY (codigo_PERSONAJES , codigo_PELICULAS),
  FOREIGN KEY (codigo_PELICULAS) REFERENCES PELICULAS(codigo)
);

CREATE TABLE PLANETAS (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  galaxia varchar(20) DEFAULT NULL,
  nombre varchar(20) DEFAULT NULL,
  PRIMARY KEY (codigo)
);

CREATE TABLE VISITAS (
  codigo_NAVES int(11) NOT NULL,
  codigo_PLANETAS int(11) NOT NULL,
  codigo_PELICULAS int(11) NOT NULL,
  PRIMARY KEY (codigo_NAVES, codigo_PLANETAS, codigo_PELICULAS),
  FOREIGN KEY (codigo_PLANETAS) REFERENCES PLANETAS(codigo),
  FOREIGN KEY (Codigo_PELICULAS) REFERENCES PELICULAS(codigo)
);
